#!/usr/bin/ruby

require 'net/http'
require 'json'

##Detect user face for verification
#uri = URI('https://australiaeast.api.cognitive.microsoft.com/face/v1.0/detect')
#uri.query = URI.encode_www_form({})
#request = Net::HTTP::Post.new(uri.request_uri)
## Request headers
#request['Content-Type'] = 'application/json'	
#request['Ocp-Apim-Subscription-Key'] = '7e97f1fd43fe4e71baab49797e658ad8'
## Request body
#request.body = '{"url":"https://s3.amazonaws.com/team27testingfaces/0_1.jpg"}'
#response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|http.request(request)
#end
#
#user_face_ID = JSON.parse(response.body)[0]['faceId']
user_face_ID = "e7eae8e8-f09b-4857-83ec-4108b89d417d"

temp_face_IDs = Array["e7eae8e8-f09b-4857-83ec-4108b89d417d","16382b39-e962-4f35-85dc-a7fcd0fd2b03","c87fda8b-2dd2-4a26-8a97-4f78261fd773","b6b87cf2-01fd-4733-bef8-b0fcc33b21d8","03a16545-98f4-4fee-980b-e3f49fb02b68","9f05476f-44bd-40d2-9a9a-548fecfa09d3","80a2d03c-f3d7-40cd-8a23-00e97c41357c","8c6e461f-f9df-4ecc-ac76-4d4fc9660c58","de5cb6b1-98e0-4fd6-9b6d-83e2d7849cf2","8ce4fe7c-99bd-4447-bce4-fc9226d58130"]
for i in 0..9
#	image_to_compare = "#{i}_1.jpg"
#	
#	uri = URI('https://australiaeast.api.cognitive.microsoft.com/face/v1.0/detect')
#	uri.query = URI.encode_www_form({})
#	request = Net::HTTP::Post.new(uri.request_uri)
#	# Request headers
#	request['Content-Type'] = 'application/json'
#	
#	request['Ocp-Apim-Subscription-Key'] = '7e97f1fd43fe4e71baab49797e658ad8'
#	# Request body
#	request.body = "{\"url\":\"https://s3.amazonaws.com/team27testingfaces/#{image_to_compare}\"}"
#
#	response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|http.request(request)
#	end
#	temp_face_ID = JSON.parse(response.body)[0]['faceId']
#	puts temp_face_ID
	
	
	
	uri = URI('https://australiaeast.api.cognitive.microsoft.com/face/v1.0/verify')
	uri.query = URI.encode_www_form({})
	request = Net::HTTP::Post.new(uri.request_uri)
	# Request headers
	request['Content-Type'] = 'application/json'
	request['Ocp-Apim-Subscription-Key'] = '7e97f1fd43fe4e71baab49797e658ad8'
	# Request body
	request.body = "{\"faceId1\":\"#{user_face_ID}\",
	\"faceId2\":\"#{temp_face_IDs[i]}\"}"

	response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|http.request(request)
	end
	
	puts response.body
	
end
