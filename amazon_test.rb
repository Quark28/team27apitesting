#!/usr/bin/ruby


require 'dotenv'
Dotenv.load
require 'aws-sdk'

client = Aws::Rekognition::Client.new

user_image = "0_1.jpg"

#for loop thru target images
for i in 0..9
	image_to_compare = "#{i}_1.jpg"
	response = client.compare_faces({
		similarity_threshold: 90, 
		source_image: {
			s3_object: {
				bucket: "team27testingfaces", 
				name: user_image,
			}, 
		}, 
		target_image: {
			s3_object: {
				bucket: "team27testingfaces", 
				name: image_to_compare, 
			}, 
		}, 
	})
	if response.face_matches.length > 0
		puts "Confidence faces match is #{response.face_matches[0].face.confidence}"
	elsif response.unmatched_faces.length > 0 
		puts "Confidence faces don't match is #{response.unmatched_faces[0].confidence}" 
	end
end
