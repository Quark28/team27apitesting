#!/usr/bin/ruby

require 'net/http'
require 'json'

#enroll user request
uri = URI('https://api.kairos.com/enroll')
uri.query = URI.encode_www_form({})
request = Net::HTTP::Post.new(uri.request_uri)
# Request headers
request['Content-Type'] = 'application/json'
request['app_id'] = '017ff843'
request['app_key'] = '52de6526de1d2da9267e07934ebea93c'
# Request body
request.body = '{"image":"https://s3.amazonaws.com/team27testingfaces/0_1.jpg","subject_id":"TestUser", "gallery_name":"TestGallery"}'

response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|http.request(request)
end

puts response


#verify request
#adds a body in the loop
uri = URI('https://api.kairos.com/verify')
	uri.query = URI.encode_www_form({})
	request = Net::HTTP::Post.new(uri.request_uri)
	# Request headers
	request['Content-Type'] = 'application/json'
	request['app_id'] = '017ff843'
	request['app_key'] = '52de6526de1d2da9267e07934ebea93c'

for i in 0..9
	image_to_compare = "#{i}_1.jpg"
	
	# Request body
	request.body = "{\"image\":\"https://s3.amazonaws.com/team27testingfaces/#{image_to_compare}\", \"gallery_name\":\"TestGallery\",\"subject_id\":\"TestUser\"}"

	response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|http.request(request)
	end
	
	puts JSON.parse(response.body)['images'][0]['transaction']['confidence']
end
